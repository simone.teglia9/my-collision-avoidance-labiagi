#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <cmath>

/**
 * Cono visivo di 20 gradi: 10 a dx e 10 a sx
 * Ranges da i=500 a i=580
 *
 **/

ros::Subscriber vel_sub;
ros::Publisher pub;
geometry_msgs::Twist msg;

int navigation_ok = 1;
int rotating = 0;
float angular_speed = (10*2*3.14)/360;
float safe_angle = 10;

int check_intorno(float distance[], int i,const sensor_msgs::LaserScan::ConstPtr& scan){
    if (i > 20 && i < 60){
        for (int j = i-19; j < i+19; j++){
            if (distance[j] < 0.6) return 0;
        }
    }else if (i < 20){
        for (int j = 0; j < i+ 19; j++){
            if (distance[j] < 0.6) return 0;
        }
    }else if (i > 60){
        for (int j = i-19; j < 80; j++){
            if (distance[j] < 0.6) return 0;
        }
    }
    return 1;
}


void laserCallBack(const sensor_msgs::LaserScan::ConstPtr& scan){
    float front_distance = scan->ranges[540];
    if (navigation_ok && !rotating){
        float distance[80];

        for (int k = 0; k < 80; k++){
            distance[k] = scan->ranges[k + 500];
        }

        float min_distance_k = 30.0;
        float max_distance_k = 0.0;

        int min_distance_k_index = 0;
        int max_distance_k_index = 0;

        for (int i = 0; i < 80; i ++){
            if (distance[i] < min_distance_k){
                min_distance_k = distance[i];
                min_distance_k_index = i + 500;
            }
            else if (distance[i] > max_distance_k){
                if(check_intorno(distance, i, scan)){
                    max_distance_k = distance[i];
                    max_distance_k_index = i + 500;
                }
                
            }
        }

        ROS_INFO("max distance = %f, range = %d", max_distance_k, max_distance_k_index);
        ROS_INFO("min distance = %f, range = %d", min_distance_k, min_distance_k_index);
        
        if (min_distance_k < 0.6){
            if (min_distance_k < 0.3) navigation_ok = 0;
            rotating = 1;
            int lower_bound = max_distance_k < 1.0 ? 0 : 180;
            int higher_bound = max_distance_k < 1.0 ? 1080: 90;
            ROS_INFO("lower bound = %d, higher bound = %d", lower_bound, higher_bound);
            for (int i = lower_bound;  i < higher_bound; i++){
                if (scan->ranges[i] > max_distance_k){
                    max_distance_k = scan->ranges[i];
                    max_distance_k_index = i;
                    ROS_INFO("Found new max_distance");
                }
            }
            //MIN DISTANCE INDEX DEVE ESSERE SEMPRE "DISTANTE" ALMENO 20 DAL MAX DISTANCE INDEX
            if (min_distance_k_index > max_distance_k_index){
                if (min_distance_k_index - max_distance_k_index < 30){
                    min_distance_k_index += (30 - (min_distance_k_index - max_distance_k_index));
                }
            }
            else {
                if (max_distance_k_index - min_distance_k_index < 30){
                    min_distance_k_index += 30 - (max_distance_k_index - min_distance_k_index);
                }
            }
            ROS_INFO("New min distance index = %d", min_distance_k_index);
            float angle = (max_distance_k_index + safe_angle - 540)*0.25;
            ROS_INFO("Angle = [%f]", angle);

            msg.linear.x = 0;
            msg.angular.z = angle > 0 ? angular_speed : - angular_speed;

            float current_angle = 0;
            float goal_angle = abs(angle + 10 ) * 2 *3.14 / 360;

            ROS_INFO("Starting rotation");

            double t0 = ros::Time::now().toSec();

            while(current_angle < goal_angle){
                pub.publish(msg);
                double t1 = ros::Time::now().toSec();
                current_angle = angular_speed * (t1-t0);
            }

            rotating = 0;

            ROS_INFO("Rotation finished");

            msg.angular.z = 0;
            msg.linear.x = 0.6;

            pub.publish(msg);

            navigation_ok = 1;

        }
    }
    
    /**
    for(int i = 500; i < 580; i++){
        float distance = scan->ranges[i];
        if (distance < 0.7 && !rotating){
            navigation_ok = 0;
            rotating = 1;
            ROS_INFO("OBSTACLE!");
            float max_distance = 0.0;
            int max_distance_index = 460;
            for (int j = 360; j < 720; j++){
                if (scan->ranges[j] > max_distance){
                    max_distance = scan->ranges[j];
                    max_distance_index  = j;
                }
            }
            ROS_INFO("max_distance_index = [%d]", max_distance_index);
            float angle = (max_distance_index - 540)*0.25;
            ROS_INFO("Angle = [%f]", angle);

            msg.linear.x = 0;
            msg.angular.z = angle > 0 ? angular_speed : - angular_speed;

            float current_angle = 0;
            float goal_angle = abs(angle + safe_angle) * 2 *3.14 / 360;

            ROS_INFO("Starting rotation");

            double t0 = ros::Time::now().toSec();

            while(current_angle < goal_angle){
                pub.publish(msg);
                double t1 = ros::Time::now().toSec();
                current_angle = angular_speed * (t1-t0);
            }

            rotating = 0;

            ROS_INFO("Rotation finished");

            msg.angular.z = 0;
            msg.linear.x = 0.6;

            pub.publish(msg);

            navigation_ok = 1;

            break;
        }
    }
    **/
}


int main(int argc, char* argv[]){
    ros::init(argc, argv, "my_collision_avoidance_node");
    ros::NodeHandle nh;

    ROS_INFO("Hello collision avoidance!!!");

    pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
    vel_sub = nh.subscribe<sensor_msgs::LaserScan>("/base_scan", 1, laserCallBack);

    msg.linear.x = 0.6;

    while(ros::ok()){
        if (navigation_ok){
            pub.publish(msg);
        }
        ros::spinOnce();
    }
}
