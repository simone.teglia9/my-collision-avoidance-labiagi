Collision Avoidance project per LABIAGI
Teglia Simone

# COME UTILIZZARE IL NODO


1. Nel terminale posizionarsi nel workspace ros
2. Scaricare e inserire la cartella del nodo nel workspace
3. Effettuare il source del terminale nel setup del workspace con ``` source devel/setup.bash ```
4. Effettuare la build del workspace con ``` catkin build ``` 
5. Lanciare ros con il comando ``` roscore ```
6. In un altro tab del terminale (sempre con stesso source) lanciare stageros e una mappa con il comando ``` rosrun stage_ros stageros {nome mappa} ```
7. In un altro tab del terminale (sempre con stesso source) lanciare il nodo my_collision_avoidance con il comando ``` rosrun my_collision_avoidance my_collision_avoidance_node ```
